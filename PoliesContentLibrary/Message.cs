﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PoliesContentLibrary {
	public class Message {
		public int id;
		public string message;
		public int weight;
		public Dictionary<string, int> choices;
	}
}
