# Polies

This is an artgame about LGBT issues.

## Object

Become as happy as possible in 30 days by interacting with people and forming
relationships

## The Catch

You don't know your sexuality from the getgo. You don't know your gender either.
Or your preferences concerning polyamory. You need to discover all of this by
playing.

## Other Factors

### The Town

The town is randomly generated as well. You don't know where your perfect match
is, or if they're even out there. Multiple playthroughs will be different every
time.

### The Townsfolk

Not everyone may be willing to accept you for who you are. Some people might get
hostile with you. You may fall in love with someone who isn't attracted to you.
The townsfolk are all randomly generated as well, so you never know what you'll
encounter.

### Events

Certain events may randomly occur on different days. Someone might kill
themselves. Someone else might have/adopt a baby. Maybe the town will get
visitors that try and persuade other townsfolk into thinking a certain way.

You never know what'll happen.

### Rumors

Polies talk. If you reveal yourself to the wrong person, you might make a lot of
enemies really quickly! Or you might instead make a lot of allies. Be careful
with what you disclose to whom, or at least be prepared to face the
consequences.

# Building

**Don't do it! It's not done yet!**

# Contributing

I'd much rather work on this myself, however I'm more than willing to discuss
issues, accept patches, add/change/remove features, etc. File an issue or a pull
request and I'll see what I can do. 

Primarily this is a learning project for me, so I get the final say about any
requests.
