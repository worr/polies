﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Polies.Polies {
	public class Acceptance {
		public enum Level { None, Enemy, Mid, Friend, Max };

		private Dictionary<Polie.FlavorType, Level> acceptance;

		public Level Homosexual {
			get {
				return acceptance[Polie.FlavorType.Homosexual];
			}
			set {
				acceptance[Polie.FlavorType.Homosexual] = value;
			}
		}

		public Level Bisesxual {
			get {
				return acceptance[Polie.FlavorType.Bisexual];
			}
			set {
				acceptance[Polie.FlavorType.Bisexual] = value;
			}
		}

		public Level Trans {
			get {
				return acceptance[Polie.FlavorType.Trans];
			}
			set {
				acceptance[Polie.FlavorType.Trans] = value;
			}
		}

		public Level Genderqueer {
			get {
				return acceptance[Polie.FlavorType.Genderqueer];
			}
			set {
				acceptance[Polie.FlavorType.Genderqueer] = value;
			}
		}

		public Dictionary<Polie.FlavorType, Level> baseObj {
			get {
				return acceptance;
			}
		}

		public Level Polyamorous {
			get {
				return acceptance[Polie.FlavorType.Polyamorous];
			}
			set {
				acceptance[Polie.FlavorType.Polyamorous] = value;
			}
		}

		public Level Asexual {
			get {
				return acceptance[Polie.FlavorType.Asexual];
			}
			set {
				acceptance[Polie.FlavorType.Asexual] = value;
			}
		}

		public Acceptance() {
			Random rand = new Random();
			acceptance = new Dictionary<Polie.FlavorType, Level>();
			acceptance.Add(Polie.FlavorType.Homosexual, (Level)rand.Next((int)Level.Max + 1));
			acceptance.Add(Polie.FlavorType.Bisexual, (Level)rand.Next((int)Level.Max + 1));
			acceptance.Add(Polie.FlavorType.Trans, (Level)rand.Next((int)Level.Max + 1));
			acceptance.Add(Polie.FlavorType.Genderqueer, (Level)rand.Next((int)Level.Max + 1));
			acceptance.Add(Polie.FlavorType.Polyamorous, (Level)rand.Next((int)Level.Max + 1));
			acceptance.Add(Polie.FlavorType.Asexual, (Level)rand.Next((int)Level.Max + 1));
		}

		public Acceptance(Dictionary<Polie.FlavorType, Level> seed)
			: this() {
			foreach (Polie.FlavorType k in seed.Keys) {
				if (seed[k] > Level.Max || seed[k] < Level.None)
					throw new ArgumentOutOfRangeException();
				acceptance[k] = seed[k];
			}
		}

		public override string ToString() {
			string ret = "";
			foreach (KeyValuePair<Polie.FlavorType, Level> kv in acceptance) {
				ret += kv.ToString();
			}

			return ret;
		}
	}
}