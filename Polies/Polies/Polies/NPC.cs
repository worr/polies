﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework.Content;

namespace Polies.Polies {
	/// <summary>
	/// All of the Polies in the game that aren't the player
	/// </summary>
	public class NPC : Polie, Interactable {

		public NPC() : base() { }

		/// <summary>
		/// Creates a new NPC and sets their initial perception fo you to be "normal"
		/// </summary>
		/// <param name="cm">The ContentManager object from which to load the template</param>
		/// <param name="you">The Player (or other NPC) from which to base their color perception</param>
		public NPC(ContentManager cm, Polie you) : base() {
			PoliesContentLibrary.Polie tmp = cm.Load<PoliesContentLibrary.Polie>("Polies/Templates/StraightCis");
			perception = new NPC();
			perception.ReadXML(tmp);
			perception.Color = you.Color;
			perception.InnerColor = perception.Color;
		}

		/// <summary>
		/// Gets the NPC's perception of the player
		/// </summary>
		private Polie perception;
		public Polie Perception {
			get {
				return perception;
			}
			set {
				perception = value;
			}
		}

		/// <summary>
		/// Talk to the NPC
		/// </summary>
		public void Interact() {
		}
	}
}
