﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Polies.Polies {
	public class InterestMatrix : IEnumerable<Interest> {
		public enum Main { Science, Food, Music, Movies, Sports, Reading, Technology, Crafts, End };
		public enum Level { None, Interested, Passionate = 4, End };

		public string[][] subs = new string[][]{
									 new string[]{ "biology", "chemistry", "physics", "astronomy", "astrophysics", "geology", "zoology" },
									 new string[]{ "ethiopian", "korean", "chinese", "japanese", "mexican", "fast food", "vietnamese" },
									 new string[]{ "rock", "pop", "rap", "indie", "alternative", "electronic", "dubstep" },
									 new string[]{ "scifi", "horror", "action", "thrillers", "suspense", "drama", "documentary" },
									 new string[]{ "basketball", "baseball", "rugby", "cricket", "frisbee", "football", "soccer" },
									 new string[]{ "scifi", "horror", "fiction", "non-fiction", "reference", "mystery", "poetry" },
									 new string[]{ "videogames", "programming", "consumer electronics", "systems administration", "networking", "social media", "reddit" },
									 new string[]{ "knitting", "sewing", "models", "painting", "drawing", "jewelry making", "scrapbooking" }
								 };

		private Dictionary<Main, Interest> interests;

		public InterestMatrix() {
			interests = new Dictionary<Main, Interest>();
			for (int i = 0; i < (int)Main.End; i++) {
				interests[(Main)i] = new Interest(subs[i]);
			}
		}

		IEnumerator IEnumerable.GetEnumerator() {
			for (int i = 0; i < (int)Main.End; i++) {
				yield return interests[(Main)i];
			}
		}

		IEnumerator<Interest> IEnumerable<Interest>.GetEnumerator() {
			for (int i = 0; i < (int)Main.End; i++) {
				yield return interests[(Main)i];
			}
		}
	}
}
