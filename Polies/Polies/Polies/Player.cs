﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;

namespace Polies.Polies {
	/// <summary>
	/// Player is the Polie that the human controls
	/// </summary>
	public class Player : Polie {

		public Player(): base() { }

		/// <summary>
		/// Represents what the player thinks about themselves.
		/// </summary>
		private Polie selfImage;
		public Polie SelfImage {
			get {
				return selfImage;
			}
			set {
				selfImage = value;
			}
		}

		/// <summary>
		/// The player's happiness score
		/// </summary>
		private int happiness;
		public int Happiness {
			get {
				return happiness;
			}
			set {
				happiness = value;
			}
		}

		/// <summary>
		/// A dictionary of NPCs and an integer representation of their friendliness with them
		/// </summary>
		private Dictionary<NPC, int> relationships;
		public Dictionary<NPC, int> Relationships {
			get {
				return relationships;
			}
		}

		/// <summary>
		/// Sets up a new player
		/// </summary>
		/// <param name="cm">The ContentManager object which reads in the XML for the initial self image</param>
		public Player(ContentManager cm)
			: base() {
				happiness = 0;
				selfImage = new Player();
				selfImage.ReadXML(cm.Load<PoliesContentLibrary.Polie>("Polies/Templates/StraightCis"));
				relationships = new Dictionary<NPC, int>();
		}

		/// <summary>
		/// Called after a successful interaction with a Polie
		/// </summary>
		/// <param name="other">other Polie in interaction</param>
		/// <param name="value">Amount interaction was worth</param>
		/// <returns>New relationship level</returns>
		public int SuccessfulInteraction(NPC other, int value) {
			return relationships[other] += value;
		}

		/// <summary>
		/// Called after a successful, insignificant with a Polie
		/// </summary>
		/// <param name="other">other Polie in interaction</param>
		/// <returns>New relationship level</returns>
		public int SuccessfulInteraction(NPC other) {
			// TODO: Make that a constant somewhere
			return SuccessfulInteraction(other, 5);
		}
	}
}
