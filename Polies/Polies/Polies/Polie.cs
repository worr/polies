﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Polies.Polies {
	/// <summary>
	/// This is a Polie: our delightful little townsfolk in our game
	/// </summary>
	public abstract class Polie : ReadsXML<PoliesContentLibrary.Polie> {
		public enum Sexuality { Bisexual, Heterosexual, Homosexual, None };
		public enum ColorType { Green, GreenYellow, Both, YellowGreen, Yellow };
		public enum FlavorType { Bisexual, Homosexual, Genderqueer, Trans, Heterosexual, Polyamorous, Asexual };
		public enum Hostility { None, Cold, Angry, Violent };
		public const int MAX_VIOLENCE = 5;

		/// <summary>
		/// The Polie's sexual preference
		/// </summary>
		private Sexuality sexualPreference;
		public Sexuality SexualPreference {
			get {
				return sexualPreference;
			}
			set {
				sexualPreference = value;
			}
		}

		/// <summary>
		/// The Polie's color - analogous to sex in this case
		/// </summary>
		private ColorType color;
		public ColorType Color {
			get {
				return color;
			}
			set {
				color = value;
			}
		}

		/// <summary>
		/// The Polie's internal color - analogous to gender in this case
		/// </summary>
		private ColorType innerColor;
		public ColorType InnerColor {
			get {
				return innerColor;
			}
			set {
				innerColor = value;
			}
		}

		/// <summary>
		/// How violent this Polie is. Scale of 0 - 5
		/// </summary>
		private int violence;
		public int Violence {
			get {
				return violence;
			}
			set {
				violence = value;
			}
		}

		/// <summary>
		/// Flavor is a really convenient way to pick out what letter of QUILTBAG the Polie is
		/// </summary>
		private List<FlavorType> flavor;
		public List<FlavorType> Flavor {
			get {
				return flavor;
			}
			set {
				flavor = value;
			}
		}

		/// <summary>
		/// An object that represents the different levels of acceptance a Polie has towards
		/// flavors of Polies
		/// </summary>
		private Acceptance accept;
		public Acceptance Accept {
			get {
				return accept;
			}

			set {
				accept = value;
			}
		}

		/// <summary>
		/// The coordinate of the Polie
		/// </summary>
		private Vector2 location;
		public Vector2 Location {
			get {
				return location;
			}
			set {
				location = value;
			}
		}

		/// <summary>
		/// Whether or not the Poly is monogamous
		/// </summary>
		private bool monogamous;
		public bool Monogamous {
			get {
				return monogamous;
			}
			set {
				monogamous = value;
			}
		}

		/// <summary>
		/// The object that represents the Polie's interests
		/// </summary>
		private InterestMatrix im;
		public InterestMatrix InterestMatrix {
			get {
				return im;
			}
		}

		/// <summary>
		/// Let's make a new random Polie!
		/// </summary>
		public Polie() {
			Dictionary<FlavorType, Acceptance.Level> seed = new Dictionary<FlavorType, Acceptance.Level>();
			Random rand = new Random();

			violence = rand.Next(MAX_VIOLENCE);
			color = (ColorType)rand.Next((int)ColorType.Yellow + 1);
			sexualPreference = (Sexuality)rand.Next((int)Sexuality.None + 1);

			flavor = new List<FlavorType>(2);
			im = new InterestMatrix();

			switch (sexualPreference) {
				case Sexuality.Bisexual:
					Flavor.Add(FlavorType.Bisexual);
					seed.Add(FlavorType.Bisexual, Acceptance.Level.Max);
					break;
				case Sexuality.Heterosexual:
					Flavor.Add(FlavorType.Heterosexual);
					break;
				case Sexuality.Homosexual:
					Flavor.Add(FlavorType.Homosexual);
					seed.Add(FlavorType.Homosexual, Acceptance.Level.Max);
					break;
				case Sexuality.None:
					Flavor.Add(FlavorType.Asexual);
					seed.Add(FlavorType.Asexual, Acceptance.Level.Max);
					break;
			}

			switch (rand.Next(10)) {
				case 0:
					Flavor.Add(FlavorType.Trans);
					innerColor = color;
					seed.Add(FlavorType.Trans, Acceptance.Level.Max);
					break;
				case 1:
					Flavor.Add(FlavorType.Genderqueer);
					innerColor = ColorType.Both;
					seed.Add(FlavorType.Genderqueer, Acceptance.Level.Max);
					break;
				default:
					innerColor = color;
					break;
			}

			switch (rand.Next(10)) {
				case 0:
					monogamous = false;
					Flavor.Add(FlavorType.Polyamorous);
					seed.Add(FlavorType.Polyamorous, Acceptance.Level.Max);
					break;
				default:
					monogamous = true;
					break;
			}

			accept = new Acceptance(seed);
		}

		/// <summary>
		/// Gets the hostility of one Polie towards another
		/// </summary>
		/// <param name="other">Polie to check against</param>
		/// <returns>The level of hostility that this Polie feels towards the other</returns>
		public Hostility HostilityTowards(Polie other) {
			// Set lowest acceptance to max
			Acceptance.Level lowest = Acceptance.Level.Max;

			foreach (FlavorType flav in other.Flavor) {
				if (flav == FlavorType.Heterosexual) continue;
				// If acceptance is below mid, change the lowest acceptance level
				if (accept.baseObj[flav] < Acceptance.Level.Mid) {
					lowest = (Acceptance.Level)Math.Min((int)lowest, (int)accept.baseObj[flav]);
				}
			}

			if (lowest == Acceptance.Level.None && violence > 4) {
				return Hostility.Violent;
			} else if (lowest == Acceptance.Level.None) {
				return Hostility.Angry;
			}

			if (lowest == Acceptance.Level.Enemy && violence > 4) {
				return Hostility.Angry;
			} else if (lowest == Acceptance.Level.Enemy) {
				return Hostility.Cold;
			}

			return Hostility.None;
		}

		/// <summary>
		/// This reads an object generate from an XML file at compile time.
		/// </summary>
		/// <param name="p">The PoliesContentLibrary.Polie object generated from an XML file</param>
		public void ReadXML(PoliesContentLibrary.Polie p) {
			if (p.sexualpreference > (int)Sexuality.None) {
				throw new ArgumentOutOfRangeException("sexualPreference");
			}
			sexualPreference = (Sexuality)p.sexualpreference;

			if (p.color > (int)ColorType.Yellow) {
				throw new ArgumentOutOfRangeException("color");
			}
			color = (ColorType)p.color;

			if (p.innercolor > (int)ColorType.Yellow) {
				throw new ArgumentOutOfRangeException("innerColor");
			}
			innerColor = (ColorType)p.innercolor;

			violence = p.violence;

			flavor = new List<FlavorType>(p.flavor.Length);
			foreach (int i in p.flavor) {
				if (i > (int)FlavorType.Asexual) {
					throw new ArgumentOutOfRangeException("flavorType");
				}

				flavor.Add((FlavorType)i);
			}

			monogamous = p.monogamous != 0;

			// We don't need to bounds check the values since the constructor does this
			// for us
			Dictionary<FlavorType, Acceptance.Level> seed = new Dictionary<FlavorType, Acceptance.Level>();
			foreach (KeyValuePair<int, int> kv in p.acceptance) {
				if (kv.Key > (int)FlavorType.Asexual) {
					throw new ArgumentOutOfRangeException("acceptance");
				}

				seed.Add((FlavorType)kv.Key, (Acceptance.Level)kv.Value);
				accept = new Acceptance(seed);
			}
		}

		/// <summary>
		/// Determines if the Polie is compatible with another Polie
		/// </summary>
		/// <param name="other">Polie to check compatibility with</param>
		/// <returns>Whether the Player is capable of liking the NPC</returns>
		public bool Compat(Polie other) {
			if (this.HostilityTowards(other) != Hostility.None)
				return false;

			if (this.SexualPreference == Sexuality.None)
				return false;

			// There is a slight problem with this, in which people genderqueer/intersex people always one particular way
			if (this.SexualPreference == Sexuality.Homosexual)
				return ((int)this.Color > (int)ColorType.Both ?
					(int)other.Color > (int)ColorType.Both :
					(int)other.Color < (int)ColorType.Both);

			if (this.SexualPreference == Sexuality.Heterosexual)
				return ((int)this.Color > (int)ColorType.Both ?
					(int)other.Color < (int)ColorType.Both :
					(int)other.Color > (int)ColorType.Both);

			return true;
		}
	}
}
