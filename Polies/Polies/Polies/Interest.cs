﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Polies.Polies {
	public class Interest {
		private Dictionary<string, InterestMatrix.Level> subs;
		public string[] SubTypes {
			get {
				string[] ret = {};
				int i = 0;

				foreach (string k in subs.Keys) {
					ret[i] = k;
					i++;
				}

				return ret;
			}
		}

		public int Overall {
			get {
				int ret = 0;
				foreach (InterestMatrix.Level i in subs.Values) {
					ret += (int)i;
				}

				return ret;
			}
		}

		public List<string> Passionate {
			get {
				Dictionary<string, InterestMatrix.Level> temp = (Dictionary<string, InterestMatrix.Level>)subs.Where((item) => { return item.Value == InterestMatrix.Level.Passionate; });
				return temp.Keys.ToList<string>();
			}
		}

		public List<string> Interested {
			get {
				Dictionary<string, InterestMatrix.Level> temp = (Dictionary<string, InterestMatrix.Level>)subs.Where((item) => { return item.Value == InterestMatrix.Level.Interested; });
				return temp.Keys.ToList<string>();
			}
		}

		public Interest(string[] subs) {
			Random rand = new Random();
			this.subs = new Dictionary<string, InterestMatrix.Level>();

			int r;

			foreach (string sub in subs) {
				switch (r = rand.Next((int)InterestMatrix.Level.End)) {
					case 2:
					case 3:
						r = 1;
						goto default;
					default:
						this.subs[sub] = (InterestMatrix.Level)r;
						break;
				}
			}
		}

		public InterestMatrix.Level RaiseInterest(string interest) {
			if (subs[interest] != InterestMatrix.Level.Passionate)
				subs[interest] = (InterestMatrix.Level)((int)subs[interest] * 3 + 1);
			return subs[interest];
		}

		public InterestMatrix.Level LoseInterest(string interest) {
			if (subs[interest] != InterestMatrix.Level.None)
				subs[interest] = (InterestMatrix.Level)(((int)subs[interest] - 1) / 3);
			return subs[interest];
		}
	}
}
