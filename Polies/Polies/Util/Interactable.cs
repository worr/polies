﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Polies {
	interface Interactable {
		void Interact();
	}
}
