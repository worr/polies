﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Polies {
	/// <summary>
	/// This interface defines methods for objects that must be run everyday
	/// </summary>
	public interface Runnable {
		void Run();
	}
}
