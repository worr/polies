﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Polies {
	interface ReadsXML<T> {
		void ReadXML(T obj);
	}
}
