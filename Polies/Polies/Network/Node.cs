﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Polies.Polies;

namespace Polies.Network {
	/// <summary>
	/// A Node in the Polie Network
	/// </summary>
	public class Node {

		/// <summary>
		/// The Polie that the Node represents
		/// </summary>
		private NPC p;
		public NPC Polie {
			get {
				return p;
			}
		}

		/// <summary>
		/// The list of connections elsewhere in the graph
		/// </summary>
		private LinkedList<Node> connections;
		public LinkedList<Node> Connections {
			get {
				return connections;
			}
		}

		/// <summary>
		/// Builds a Node around a Polie
		/// </summary>
		/// <param name="p">The Polie to make the Node from</param>
		public Node(NPC p) {
			this.p = p;
			connections = new LinkedList<Node>();
		}

		/// <summary>
		/// Adds a connection to this Node
		/// </summary>
		/// <param name="other">The Node to connect this Node to</param>
		public void AddConnection(Node other) {
			connections.AddLast(other);
		}
	}
}
