﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Polies.Network {
	/// <summary>
	/// The Rumor Manager disseminates rumors to the NPCs
	/// </summary>
	public sealed class RumorManager : Runnable {
		private PolieNetwork network;
		private LinkedList<Rumor> queue;

		/// <summary>
		/// Creates a new RumorManager
		/// </summary>
		/// <param name="pn">The PolieNetwork for the game</param>
		public RumorManager(PolieNetwork pn) { 
			network = pn;
			queue = new LinkedList<Rumor>();
		}

		/// <summary>
		/// Adds a Rumor to the queue
		/// </summary>
		/// <param name="r">The Rumor to append to the queue</param>
		public void Add(Rumor r) {
			queue.AddLast(r);
		}

		/// <summary>
		/// Runs the Manager and disseminates the rumors
		/// </summary>
		public void Run() {
			foreach (Rumor r in queue) {
				LinkedList<Node> newState = new LinkedList<Node>();

				foreach (Node n in r.State) {
					foreach (Node connection in n.Connections) {
						connection.Polie.Perception = r.You;
						newState.AddLast(connection);
					}
				}

				r.State = newState;
				r.DecrementTTL();
			}

			Cleanup();
		}

		private void Cleanup() {
			LinkedList<Rumor> newRumors = new LinkedList<Rumor>(queue);
			foreach (Rumor r in queue) {
				if (r.TTL <= 0) {
					newRumors.Remove(r);
				}
			}

			queue = newRumors;
		}
	}
}
