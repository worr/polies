﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Polies.Polies;

namespace Polies.Network {
	/// <summary>
	/// The graph of the relationships between different Polies. It's incredibly simple right now.
	/// </summary>
	public sealed class PolieNetwork {

		/// <summary>
		/// Creates a new network of Polies
		/// </summary>
		public PolieNetwork() {
			network = new LinkedList<Node>();
		}

		private LinkedList<Node> network;

		/// <summary>
		/// Add a Polie to the PolieNetwork
		/// </summary>
		/// <param name="p">The Polie to add</param>
		public void Add(NPC p) {
			network.AddLast(new Node(p));
		}

		/// <summary>
		/// Resolves the tree and makes connections from the nodes.
		/// 
		/// This *must* be run after adding all of the connections
		/// </summary>
		public void Resolve() {
			foreach (Node n in network) {
				foreach (Node o in network) {
					if (n == o)
						continue;
					if (n.Polie.HostilityTowards(o.Polie) == Polie.Hostility.None &&
						o.Polie.HostilityTowards(n.Polie) == Polie.Hostility.None) {
						n.AddConnection(o);
					}
				}
			}
		}

		public Node GetNode(NPC p) {
			foreach (Node n in network) {
				if (n.Polie == p)
					return n;
			}

			throw new KeyNotFoundException("Could not find NPC");
		}
	}
}
