﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Polies.Polies;

namespace Polies.Network {
	/// <summary>
	/// A Rumor - holds the new perception of the player that the NPCs will have
	/// </summary>
	public class Rumor {
		/// <summary>
		/// Number of hops left in rumor cycle
		/// </summary>
		private int ttl;
		public int TTL {
			get {
				return ttl;
			}
		}

		/// <summary>
		/// The new image of you that people will know about
		/// </summary>
		private Polie you;
		public Polie You {
			get {
				return you;
			}
		}

		/// <summary>
		/// Last round of Polies that know the rumor currently
		/// </summary>
		private LinkedList<Node> state;
		public LinkedList<Node> State {
			get {
				return state;
			}
			set {
				state = value;
			}
		}

		/// <summary>
		/// Spawns a new Rumor with information about you and a TTL
		/// </summary>
		/// <param name="ttl">Number of hops for rumor to spread</param>
		/// <param name="you">New image of you</param>
		/// <param name="origin">Polie that started the rumor</param>
		public Rumor(int ttl, Polie you, Node origin) {
			this.you = you;
			this.ttl = ttl;

			state = new LinkedList<Node>();
			state.AddFirst(origin);
			origin.Polie.Perception = you;
		}

		/// <summary>
		/// Decrements the TTL
		/// </summary>
		/// <returns>New TTL</returns>
		public int DecrementTTL() {
			return --ttl;
		}
	}
}
