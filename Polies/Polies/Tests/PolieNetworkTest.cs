﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;

using Polies.Polies;
using Polies.Network;

namespace Polies.Tests {

	[TestFixture]
	class PolieNetworkTest {
		NPC[] npcs;

		[SetUp]
		public void Init() {
			ContentManager cm = new ContentManager(new GameServiceContainer());
			cm.RootDirectory = "C:\\Users\\will\\Documents\\Visual Studio 2010\\Projects\\Polies\\Polies\\Polies\\bin\\x86\\Debug\\Content";
			PoliesContentLibrary.Polie[] p = new PoliesContentLibrary.Polie[5];

			npcs = new NPC[5];

			for (int i = 0; i < 5; i++) {
				p[i] = cm.Load<PoliesContentLibrary.Polie>(String.Format("Polies/Test/Test{0}", i));
				npcs[i] = new NPC(cm, new NPC());
				npcs[i].ReadXML(p[i]);
			}
		}

		[Test]
		public void TestResolve() {
			PolieNetwork pn = new PolieNetwork();
			foreach (NPC n in npcs) {
				pn.Add(n);
			}

			Assert.DoesNotThrow(() => { pn.Resolve(); }, "PolieNetwork.Resolve() failed");
			Assert.DoesNotThrow(() => { pn.GetNode(npcs[0]); }, "Could not find first npc");
		}

		[Test]
		public void TestConnections() {
			PolieNetwork pn = new PolieNetwork(); ;
			foreach (NPC n in npcs) {
				pn.Add(n);
			}

			pn.Resolve();

			Node node = pn.GetNode(npcs[0]);
			Assert.AreEqual(4, node.Connections.Count);

			node = pn.GetNode(npcs[3]);
			Assert.AreEqual(2, node.Connections.Count);
			Assert.IsFalse(node.Connections.Contains(pn.GetNode(npcs[1])));
			Assert.IsFalse(node.Connections.Contains(pn.GetNode(npcs[2])));
			Assert.IsTrue(node.Connections.Contains(pn.GetNode(npcs[0])));
			Assert.IsTrue(node.Connections.Contains(pn.GetNode(npcs[4])));

			node = pn.GetNode(npcs[1]);
			Assert.AreEqual(2, node.Connections.Count);
			Assert.IsFalse(node.Connections.Contains(pn.GetNode(npcs[3])));
			Assert.IsTrue(node.Connections.Contains(pn.GetNode(npcs[2])));
			Assert.IsTrue(node.Connections.Contains(pn.GetNode(npcs[0])));
			Assert.IsFalse(node.Connections.Contains(pn.GetNode(npcs[4])));
		}
	}
}
