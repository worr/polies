﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Polies.Polies;
using NUnit.Framework;

namespace Polies.Tests {
	[TestFixture]
	class AcceptanceTest {
		[Test, Repeat(10000)]
		public void ConstructorTest() {
			Acceptance a = new Acceptance();
			Assert.GreaterOrEqual(Acceptance.Level.Max, a.Bisesxual);
			Assert.LessOrEqual(Acceptance.Level.None, a.Bisesxual);

			Assert.GreaterOrEqual(Acceptance.Level.Max, a.Genderqueer);
			Assert.LessOrEqual(Acceptance.Level.None, a.Genderqueer);

			Assert.GreaterOrEqual(Acceptance.Level.Max, a.Homosexual);
			Assert.LessOrEqual(Acceptance.Level.None, a.Homosexual);

			Assert.GreaterOrEqual(Acceptance.Level.Max, a.Trans);
			Assert.LessOrEqual(Acceptance.Level.None, a.Trans);

			Assert.GreaterOrEqual(Acceptance.Level.Max, a.Asexual);
			Assert.LessOrEqual(Acceptance.Level.None, a.Asexual);

			Assert.GreaterOrEqual(Acceptance.Level.Max, a.Polyamorous);
			Assert.LessOrEqual(Acceptance.Level.None, a.Polyamorous);
		}

		[Test, Repeat(10000)]
		public void SeedTest() {
			Dictionary<Polie.FlavorType, Acceptance.Level> seed = new Dictionary<Polie.FlavorType, Acceptance.Level>();
			seed.Add(Polie.FlavorType.Genderqueer, Acceptance.Level.Max);
			Acceptance a = new Acceptance(seed);
			Assert.AreEqual(Acceptance.Level.Max, a.Genderqueer);

			seed.Add(Polie.FlavorType.Bisexual, Acceptance.Level.Mid);
			a = new Acceptance(seed);
			Assert.AreEqual(Acceptance.Level.Mid, a.Bisesxual);
			Assert.AreEqual(Acceptance.Level.Max, a.Genderqueer);
		}

		[Test]
		public void InvalidSeedTest() {
			Dictionary<Polie.FlavorType, Acceptance.Level> seed = new Dictionary<Polie.FlavorType, Acceptance.Level>();
			Acceptance a = new Acceptance();

			seed.Add(Polie.FlavorType.Homosexual, (Acceptance.Level)10);
			Assert.Throws<ArgumentOutOfRangeException>(() => a = new Acceptance(seed));
			Assert.AreNotEqual(10, a.Homosexual);
		}
	}
}
