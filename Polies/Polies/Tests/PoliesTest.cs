﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using NUnit.Framework;
using Microsoft.Xna.Framework;

using Polies.Polies;

namespace Polies.Tests {
	[TestFixture]
	class PoliesTest {
		Polie p, other;

		[SetUp]
		public void Init() {
			p = new NPC();
			other = new NPC();
		}

		[Test, Repeat(10000)]
		public void PolieDefaultConstructorTest() {
			Assert.Greater(Polie.MAX_VIOLENCE, p.Violence);
			Assert.Less(-1, p.Violence);

			Assert.GreaterOrEqual(Polie.ColorType.Yellow, p.Color);
			Assert.LessOrEqual(Polie.ColorType.Green, p.Color);

			Assert.GreaterOrEqual(Polie.Sexuality.None, p.SexualPreference);
			Assert.Less(-1, (int)p.SexualPreference);
		}

		[Test, Repeat(1000)]
		public void PolieSaneAcceptanceTest() {
			if (p.SexualPreference == Polie.Sexuality.Bisexual) {
				Assert.AreEqual(p.Accept.Bisesxual, Acceptance.Level.Max);
			}

			if (p.SexualPreference == Polie.Sexuality.Homosexual) {
				Assert.AreEqual(p.Accept.Homosexual, Acceptance.Level.Max);
			}

			if (p.Flavor.Contains(Polie.FlavorType.Genderqueer)) {
				Assert.AreEqual(p.Accept.Genderqueer, Acceptance.Level.Max);
			}

			if (p.Flavor.Contains(Polie.FlavorType.Trans)) {
				Assert.AreEqual(p.Accept.Trans, Acceptance.Level.Max);
			}
		}

		[Test, Repeat(10000)]
		public void PolieHateTest() {
			Dictionary<Polie.FlavorType, Acceptance.Level> seed = new Dictionary<Polie.FlavorType, Acceptance.Level>();
			seed.Add(Polie.FlavorType.Bisexual, Acceptance.Level.None);
			seed.Add(Polie.FlavorType.Genderqueer, Acceptance.Level.None);
			seed.Add(Polie.FlavorType.Homosexual, Acceptance.Level.None);
			seed.Add(Polie.FlavorType.Trans, Acceptance.Level.None);
			seed.Add(Polie.FlavorType.Asexual, Acceptance.Level.None);
			seed.Add(Polie.FlavorType.Polyamorous, Acceptance.Level.None);
			Acceptance a = new Acceptance(seed);
			p.Accept = a;

			if (other.Flavor.Count == 1 && other.Flavor.Contains(Polie.FlavorType.Heterosexual))
				return;

			Assert.GreaterOrEqual(p.HostilityTowards(other), Polie.Hostility.Cold);
		}

		[Test]
		public void ReadXML() {
			ContentManager cm = new ContentManager(new GameServiceContainer());
			cm.RootDirectory = "C:\\Users\\will\\Documents\\Visual Studio 2010\\Projects\\Polies\\Polies\\Polies\\bin\\x86\\Debug\\Content";
			PoliesContentLibrary.Polie tmp = cm.Load<PoliesContentLibrary.Polie>("Polies/Templates/GayHomophobe");
			p.ReadXML(tmp);

			Assert.AreEqual(5, p.Violence);
			Assert.AreEqual(Polie.FlavorType.Homosexual, p.Flavor[0]);
			Assert.AreEqual(Polie.Sexuality.Homosexual, p.SexualPreference);
			Assert.AreEqual(Polie.ColorType.Green, p.Color);
			Assert.AreEqual(Polie.ColorType.Green, p.InnerColor);
			Assert.AreEqual(Acceptance.Level.None, p.Accept.Bisesxual);
			Assert.AreEqual(Acceptance.Level.None, p.Accept.Trans);
			Assert.AreEqual(Acceptance.Level.None, p.Accept.Homosexual);
			Assert.AreEqual(Acceptance.Level.None, p.Accept.Genderqueer);
			Assert.IsTrue(p.Monogamous);
		}
	}
}
