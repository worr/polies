﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using Polies.Polies;
using Polies.Network;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace Polies.Tests {

	[TestFixture]
	class RumorManagerTest {
		PolieNetwork pn;
		RumorManager rm;
		NPC[] npcs;
		Polie you;

		[SetUp]
		public void Init() {
			pn = new PolieNetwork();
			rm = new RumorManager(pn);
			npcs = new NPC[5];
			you = new NPC();

			ContentManager cm = new ContentManager(new GameServiceContainer());
			cm.RootDirectory = "C:\\Users\\will\\Documents\\Visual Studio 2010\\Projects\\Polies\\Polies\\Polies\\bin\\x86\\Debug\\Content";

			you.ReadXML(cm.Load<PoliesContentLibrary.Polie>("Polies/Templates/GayCis"));

			for (int i = 0; i < 5; i++) {
				npcs[i] = new NPC(cm, you);
				npcs[i].ReadXML(cm.Load<PoliesContentLibrary.Polie>(String.Format("Polies/Test/Test{0}", i)));
				pn.Add(npcs[i]);
			}

			pn.Resolve();
			rm.Add(new Rumor(2, you, pn.GetNode(npcs[3])));
		}

		[Test]
		public void TestRumorDispersion() {
			rm.Run();

			Assert.AreEqual(you.Flavor, npcs[4].Perception.Flavor);
			Assert.AreEqual(you.Flavor, npcs[0].Perception.Flavor);
			Assert.AreNotEqual(you.Flavor, npcs[2].Perception.Flavor);
			Assert.AreNotEqual(you.Flavor, npcs[1].Perception.Flavor);
		}

		[Test]
		public void TestRound2Dispersion() {
			rm.Run();
			rm.Run();

			Assert.AreEqual(you.Flavor, npcs[4].Perception.Flavor);
			Assert.AreEqual(you.Flavor, npcs[0].Perception.Flavor);
			Assert.AreEqual(you.Flavor, npcs[2].Perception.Flavor);
			Assert.AreEqual(you.Flavor, npcs[1].Perception.Flavor);
		}
	}
}
